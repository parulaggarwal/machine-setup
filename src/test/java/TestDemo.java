import org.junit.jupiter.api.Test;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestDemo {
    @Test
    public void sampleTest() {
        assertThat(1, is(equalTo(1)));
    }
}
